package demo.nab.job;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import demo.nab.job.SimpleJpaAppApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SimpleJpaAppApplication.class)
public class SimpleJpaAppApplicationTests {

	@Test
	public void contextLoads() {
	}

}
