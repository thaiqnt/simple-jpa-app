package demo.nab.job.service;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import demo.nab.job.domain.Customer;
import demo.nab.job.domain.CustomerActivity;
import demo.nab.job.domain.ProductOrder;
import demo.nab.job.domain.Product;
import demo.nab.job.repository.CustomerRepository;
import demo.nab.job.repository.ProductRepository;
import demo.nab.job.repository.OrderRepository;
import demo.nab.job.repository.ActivityRepository;

@Service
public class ProductService {
	private static final Logger log = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired
	ProductRepository productrepo;
	@Autowired
	CustomerRepository customerrepo;
	@Autowired
	OrderRepository orderrepo;
	@Autowired
	ActivityRepository activityrepo;
	
	public void insertData() throws ParseException{
		log.info("> Inserting some data...");
		customerrepo.save(new Customer("Thai"));
		
		Product p1 = new Product("HAT",new Long(100),"MAG","RED");
		Product p2 = new Product("FAN",new Long(200),"MAG","BLACK");
		Product p3 = new Product("SHORT",new Long(300),"GUCCI","RED");
		productrepo.save(p1);
		productrepo.save(p2);
		productrepo.save(p3);
		
		orderrepo.save(new ProductOrder(new Long(1), new Long(1), new Long(100)));
		
		CustomerActivity a1 = new CustomerActivity(new Long(1), "ColorSearch", "RED");
		activityrepo.save(a1);
		
		
		log.info("> Complete.");
	}
	
	public List<Customer> findAllCustomers(){
		return customerrepo.findAll();
	}
	
	public List<Product> findAllProducts(){
		return productrepo.findAll();
	}
	
	public List<ProductOrder> findAllOrders(){
		return orderrepo.findAll();
	}
	
	public List<CustomerActivity> findAllActivities(){
		return activityrepo.findAll();
	}
		
	public List<Product> findProductName(String name){
		//to support sale and marketing, all customers' activities 
		// such as searching, filtering and viewing product's details
		// need to be stored in the database.
		CustomerActivity a1 = new CustomerActivity(new Long(1), "NameSearch", name);
		activityrepo.save(a1);

		return productrepo.findProductName(name);
	}

	public List<Product> findProductPrice(Long price){
		//to support sale and marketing, all customers' activities 
		// such as searching, filtering and viewing product's details
		// need to be stored in the database.
		CustomerActivity a1 = new CustomerActivity(new Long(1), "PriceSearch", price.toString());
		activityrepo.save(a1);
		
		return productrepo.findProductPrice(price);
	}
	
	public List<Product> findProductBrand(String brand){
		//to support sale and marketing, all customers' activities 
		// such as searching, filtering and viewing product's details
		// need to be stored in the database.
		CustomerActivity a1 = new CustomerActivity(new Long(1), "BrandSearch", brand);
		activityrepo.save(a1);

		return productrepo.findProductBrand(brand);
	}
	
	public List<Product> findProductColor(String color){
		//to support sale and marketing, all customers' activities 
		// such as searching, filtering and viewing product's details
		// need to be stored in the database.
		CustomerActivity a1 = new CustomerActivity(new Long(1), "ColorSearch", color);
		activityrepo.save(a1);

		return productrepo.findProductColor(color);
	}
	
	public void placeOrder(ProductOrder order) {
		//if customer finds a product that they like, 
		// they can only place order by calling to the company's Call Centre
		// After that, the operator will enter the order for the customer
		orderrepo.save(order);

	}
}