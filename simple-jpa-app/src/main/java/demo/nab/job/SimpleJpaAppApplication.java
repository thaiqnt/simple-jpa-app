package demo.nab.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import demo.nab.job.repository.ProductRepository;
import demo.nab.job.service.ProductService;

@SpringBootApplication
public class SimpleJpaAppApplication {
	private static final Logger log = LoggerFactory.getLogger(SimpleJpaAppApplication.class);
		
	public static void main(String[] args) {
		SpringApplication.run(SimpleJpaAppApplication.class, args);
	}
	
	@Bean
	CommandLineRunner start(ProductService service){
		return args -> {
			log.info("@@ Inserting Some Product Data....");
			service.insertData();
			log.info("@@ Product data found:");
			service.findAllCustomers().forEach(entry -> log.info(entry.toString()));
			service.findAllProducts().forEach(entry -> log.info(entry.toString()));
			service.findAllOrders().forEach(entry -> log.info(entry.toString()));
			service.findAllActivities().forEach(entry -> log.info(entry.toString()));
		};
	}
}
