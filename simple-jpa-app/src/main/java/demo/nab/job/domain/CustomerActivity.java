package demo.nab.job.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class CustomerActivity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idactivity;
	private Long idcustomer;
	private String name;
	private String value;
	
		
	public CustomerActivity(Long idcustomer, String name, String value) {
		this.idcustomer = idcustomer;
		this.name = name;
		this.value = value;
	}

	public CustomerActivity() {
		
	}
	
	public Long getIdcustomer() {
		return idcustomer;
	}

	public void setIdcustomer(Long idcustomer) {
		this.idcustomer = idcustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String toString(){
		StringBuilder value = new StringBuilder("* CustomerActivityEntry(");
		value.append("IdCustomer: ");
		value.append(idcustomer);
		value.append(",Name: ");
		value.append(name);
		value.append(",Value: ");
		value.append(this.value);
		value.append(")");
		return value.toString();
	}
	
}
