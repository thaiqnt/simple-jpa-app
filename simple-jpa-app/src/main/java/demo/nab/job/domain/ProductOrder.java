package demo.nab.job.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.json.simple.JSONObject;


@Entity
public class ProductOrder {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idorder;
	
	private Long idcustomer;
	private Long idproduct;
	private Long quantity;
	
	
	
	public ProductOrder(Long idcustomer, Long idproduct, Long quantity) {
		this.idcustomer = idcustomer;
		this.idproduct = idproduct;
		this.quantity = quantity;
	}


	public ProductOrder() {
		
	}
	
	public Long getIdcustomer() {
		return idcustomer;
	}


	public void setIdcustomer(Long idcustomer) {
		this.idcustomer = idcustomer;
	}


	public Long getIdproduct() {
		return idproduct;
	}


	public void setIdproduct(Long idproduct) {
		this.idproduct = idproduct;
	}


	public Long getQuantity() {
		return quantity;
	}


	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	public String toString(){
		StringBuilder value = new StringBuilder("* ProductOrderEntry(");
		value.append("Id: ");
		value.append(idorder);
		value.append(",IdCustomer: ");
		value.append(idcustomer);
		value.append(",IdProduct: ");
		value.append(idproduct);
		value.append(",Quantity: ");
		value.append(quantity);
		value.append(")");
		return value.toString();
	}
	
	public JSONObject getJsonObject() {
		JSONObject detail = new JSONObject();

		detail.put("Idorder", idorder);
		detail.put("Idcustomer", idcustomer);
		detail.put("Idproduct", idproduct);
		detail.put("Quantity", quantity);
		
		return detail;

	}
}
