package demo.nab.job.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.json.simple.JSONObject;


@Entity
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idproduct;
	private String name;
	private Long price;
	private String brand;
	private String color;
	
	@Transient
	private SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	
	public Product(String name, Long price, String brand, String color) throws ParseException{
		this.name = name;
		this.price = price;
		this.brand = brand;
		this.color = color;
		
	}
	
	Product(){}
	
	public Long getId() {
		return idproduct;
	}
	
	public void setId(Long id) {
		this.idproduct = id;
	}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String toString(){
		StringBuilder value = new StringBuilder("* ProductEntry(");
		value.append("Id: ");
		value.append(idproduct);
		value.append(",Name: ");
		value.append(name);
		value.append(",Price: ");
		value.append(price);
		value.append(",Brand: ");
		value.append(brand);
		value.append(",Color: ");
		value.append(color);
		value.append(")");
		return value.toString();
	}
	
	public JSONObject getJsonObject() {
		JSONObject detail = new JSONObject();

		detail.put("Idproduct", idproduct);
		detail.put("Name", name);
		detail.put("Price", price);
		detail.put("Brand", brand);
		detail.put("Color", color);
		
		return detail;

	}
}
