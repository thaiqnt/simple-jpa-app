package demo.nab.job.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import demo.nab.job.domain.Product;
import demo.nab.job.domain.ProductOrder;
import demo.nab.job.service.ProductService;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Controller
public class MainController {
	public static final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	private ProductService service;
	
	private String convertProductObjects(List<Product> list) {
		JSONArray jsonArray = new JSONArray();

		JSONObject detail = new JSONObject();

		for (Product l : list) {
			// logger.info("map = " + l);
			detail = l.getJsonObject();
			jsonArray.add(detail);
		}

		return jsonArray.toJSONString();
	}

	private String convertOrderObjects(List<ProductOrder> list) {
		JSONArray jsonArray = new JSONArray();

		JSONObject detail = new JSONObject();

		for (ProductOrder l : list) {
			// logger.info("map = " + l);
			detail = l.getJsonObject();
			jsonArray.add(detail);
		}

		return jsonArray.toJSONString();
	}
	
	@GetMapping(value = "/name/{name}", produces = "application/json")
	@ResponseBody
	public String searchProductName(ModelMap model, @PathVariable("name") String name) {
		List<Product> list = service.findProductName(name);

		return convertProductObjects(list);
	}
	
	@GetMapping(value = "/price/{price}", produces = "application/json")
	@ResponseBody
	public String searchProductPrice(ModelMap model, @PathVariable("price") String price) {
		List<Product> list = service.findProductPrice(Long.valueOf(price));

		return convertProductObjects(list);
	}
	
	@GetMapping(value = "/brand/{brand}", produces = "application/json")
	@ResponseBody
	public String searchProductBrand(ModelMap model, @PathVariable("brand") String brand) {
		List<Product> list = service.findProductBrand(brand);

		return convertProductObjects(list);
	}
	
	@GetMapping(value = "/color/{color}", produces = "application/json")
	@ResponseBody
	public String searchProductColor(ModelMap model, @PathVariable("color") String color) {
		List<Product> list = service.findProductColor(color);

		return convertProductObjects(list);
	}
	
	
	@GetMapping(value = "/orders", produces = "application/json")
	@ResponseBody
	public String searchAllOrders(ModelMap model) {
		List<ProductOrder> list = service.findAllOrders();

		return convertOrderObjects(list);
	}
	
	@PostMapping(value = "/order", produces = "application/json")
	@ResponseBody
	public String placeOrder(ModelMap model, @RequestParam("idproduct") Long idproduct, @RequestParam("quantity") Long quantity) {
		//if customer finds a product that they like, 
		// they can only place order by calling to the company's Call Centre
		// After that, the operator will enter the order for the customer
		// Default customer is Thai Tran with id 1
		ProductOrder order = new ProductOrder(new Long(1), idproduct, quantity);
		
		service.placeOrder(order);

		return order.getJsonObject().toJSONString();
	}
}
