package demo.nab.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.nab.job.domain.ProductOrder;


public interface OrderRepository extends JpaRepository<ProductOrder, Long> { }
