package demo.nab.job.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import demo.nab.job.domain.Product;


public interface ProductRepository extends JpaRepository<Product, Long> { 
	@Query("select p from Product p where p.name like %?1%")
	List<Product> findProductName(String word);
	
	@Query("select p from Product p where p.price = ?1")
	List<Product> findProductPrice(Long word);
	
	@Query("select p from Product p where p.brand like %?1%")
	List<Product> findProductBrand(String word);
	
	@Query("select p from Product p where p.color like %?1%")
	List<Product> findProductColor(String word);
}
