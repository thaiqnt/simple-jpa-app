package demo.nab.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.nab.job.domain.CustomerActivity;


public interface ActivityRepository extends JpaRepository<CustomerActivity, Long> { }
