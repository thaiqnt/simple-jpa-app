package demo.nab.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.nab.job.domain.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Long> { }
