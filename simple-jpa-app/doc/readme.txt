Readme file

1. Assumption about the app:

Assume that there is only one customer Thai Tran with id 1 in the database
due to no customer registration is required.

To support sale and marketing, all customers' activities such as searching, filtering and viewing product's details need to be stored in the table Activity in the database.

Customer table

idcustomer | name
'1', 'Thai Tran'

All product data are filled with the following information:

Product table:

idproduct | name | price | brand | color

'1', 'HAT', '100', 'MAG', 'RED'
'2', 'FAN', '200', 'MAG', 'BLACK'
'3', 'SHORT', '200', 'GUCCI', 'RED'


Code structure:
The code folder is simple:

- Java code in folder src
- All documents in folder doc


2. Required step to run the demonstration app:

pull simple-jpa-app.jar from the git and be noted that the jar in doc folder,
please run the following command to start it:

java -jar doc/simple-jpa-app.jar

3. curl commands to verify the APIs:

- To Search for a product name:

curl http://127.0.0.1:8080/name/HAT

[{"Brand":"MAG","Price":100,"Color":"RED","Name":"HAT"}]

- To Search for a product price:

curl http://127.0.0.1:8080/price/100

[{"Brand":"MAG","Price":100,"Color":"RED","Name":"HAT"}]

- To Search for a product brand:

curl http://127.0.0.1:8080/brand/MAG

[{"Brand":"MAG","Price":100,"Color":"RED","Name":"HAT"},{"Brand":"MAG","Price":200,"Color":"BLACK","Name":"FAN"}]

- To Search for a product color:

curl http://127.0.0.1:8080/color/RED

[{"Brand":"MAG","Price":100,"Color":"RED","Name":"HAT"},{"Brand":"GUCCI","Price":300,"Color":"RED","Name":"SHORT"}]


- To see all orders that customers made

curl http://127.0.0.1:8080/orders

[{"Idproduct":1,"Idcustomer":1,"Quantity":100,"Idorder":1}]

- To place an order for the customer Thai Tran with id 1

curl -d "idproduct=1&quantity=12" -X POST http://127.0.0.1:8080/order

{"Idproduct":1,"Idcustomer":1,"Quantity":12,"Idorder":2}






